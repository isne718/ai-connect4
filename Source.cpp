#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void table(int grid[][7])
{
	cout << "             CONNECT FOUR          " << endl;
	cout << "-----------------------------------------" << endl << endl;
	for (int i = 5; i >= 0; i--)
	{
		for (int j = 0; j < 7; j++)
		{
			if (grid[i][j] == 1)
			{
				cout << "| X | "; //for player
			}
			else if (grid[i][j] == 2)
			{
				cout << "| O | "; //for AI
			}
			else
			{
				cout << "|   | ";
			}
		}
		cout << endl;
	}
	cout << "  1     2     3     4     5     6     7" << endl;
}

void playerTurn(int grid[][7], int turn) //player select column
{
	int column, row = 0;

	cout << "Enter column: ";
	cin >> column;

	while (grid[5][column - 1] != 0) //check full column 
	{
		cout << "This column is full";
		cout << "Enter column: ";
		cin >> column;
	}

	while (grid[row][column - 1] != 0)
	{
		row++;
	}

	grid[row][column - 1] = turn;

}

int checkWin(int grid[][7], int turn) {
	//check vertical
	for (int i = 5; i >= 3; i--) {//row
		for (int j = 0; j <= 6; j++) {//column
			if (grid[i][j] == turn && grid[i][j] == grid[i - 1][j] && grid[i][j] != 0) {
				if (grid[i - 1][j] == grid[i - 2][j]) {
					if (grid[i - 2][j] == grid[i - 3][j]) {
						return turn;
					}
				}
			}
		}
	}
	//check horizontal
	for (int i = 0; i <= 5; i++) {//row
		for (int j = 0; j <= 3; j++) {//column
			if (grid[i][j] == turn && grid[i][j] == grid[i][j + 1] && grid[i][j] != 0) {
				if (grid[i][j + 1] == grid[i][j + 2]) {
					if (grid[i][j + 2] == grid[i][j + 3]) {
						return turn;
					}
				}
			}
		}
	}
	//check diagonally left to right
	for (int i = 0; i <= 2; i++) {
		for (int j = 0; j <= 3; j++) {
			if (grid[i][j] == turn && grid[i][j] == grid[i + 1][j + 1] && grid[i][j] != 0) {
				if (grid[i + 1][j + 1] == grid[i + 2][j + 2]) {
					if (grid[i + 2][j + 2] == grid[i + 3][j + 3]) {
						return turn;
					}
				}
			}
		}
	}
	//check diagonally right to left
	for (int i = 5; i >= 3; i--) {
		for (int j = 0; j <= 3; j++) {
			if (grid[i][j] == turn && grid[i][j] == grid[i - 1][j + 1] && grid[i][j] != 0) {
				if (grid[i - 1][j + 1] == grid[i - 2][j + 2]) {
					if (grid[i - 2][j + 2] == grid[i - 3][j + 3]) {
						return turn;
					}
				}
			}
		}
	}

	//still no one win
	return 0;
}

void aiTurn(int grid[][7], int turn)
{
	int column = 0, row;

	for (int player = 1; player <= 7; player++) { //act as a player
		bool full = false;
		row = 0;

		while (grid[5][player - 1] != 0) //check full column 
		{
			full = true;
		}

		if (full == true) {
			continue;
		}

		while (grid[row][player - 1] != 0)
		{
			row++;
		}

		grid[row][player - 1] = 1;  //drop as it is player

		int win = checkWin(grid, 1);

		if (win == 1) {  //if player will be win so drop in that column
			grid[row][player - 1] = turn;
			column = player;
		}
		else { //reset value
			grid[row][player - 1] = 0;
		}
	}

	if (column == 0) { //if no one win so random it
		row = 0;
		column = rand() % 7 + 1;

		while (grid[5][column - 1] != 0) //check full column 
		{
			column = rand() % 7 + 1;
		}

		while (grid[row][column - 1] != 0)
		{
			row++;
		}

		grid[row][column - 1] = turn;


	}

}



void main()
{

	int turn = 1, round = 0, grid[6][7] = { 0 }, win=0;


	while (round <= 42)
	{
		table(grid);
		if (round % 2 == 0) { //player turn
			playerTurn(grid,1);
			win = checkWin(grid, 1);
			if (win != 0) {
				break;
			}
		}
		else //ai turn
		{
			aiTurn(grid, 2);
			win = checkWin(grid, 2);
			if (win != 0) {
				break;
			}
		}
		round++;
		system("cls");
	}

	system("cls");
	table(grid);

	if (win == 0) {
		cout << " TIE ";
	}
	else if (win == 1) {
		cout << " PLAYER WIN ";
	}
	else if (win == 2) {
		cout << " PLAYER LOSE ";
	}


}